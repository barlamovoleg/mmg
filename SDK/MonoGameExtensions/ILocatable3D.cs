using Microsoft.Xna.Framework;

namespace MonoGameExtensions
{
    public interface ILocatable3D
    {
        Vector3 Position { get; set; }
    }
}